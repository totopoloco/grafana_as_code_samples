#!/usr/bin/env bash

echo 'Deploying dashboard 1'
jsonnet -J vendor simple_prometheus/prometheus.jsonnet > ../../demo-spring-boot-concepts/observability/grafanap/home.json
echo 'Deploying dashboard 2'
jsonnet -J vendor runtime/main.libsonnet > ../../demo-spring-boot-concepts/observability/grafanae/home.json

echo 'Deployed dashboards'
