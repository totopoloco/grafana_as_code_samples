local g = import './g.libsonnet';


  g.dashboard.new('Demo dashboard')
  + g.dashboard.withUid('demo-grafonnet-demo')
  + g.dashboard.withDescription('Dashboard for Demo purposes')
  + g.dashboard.graphTooltip.withSharedCrosshair()
  + g.dashboard.time.withFrom(value='now-15m')
  + g.dashboard.withPanels([
    g.panel.timeSeries.new('Cpu usage per Spring Boot service')
    + g.panel.timeSeries.queryOptions.withTargets([
      g.query.prometheus.new(
        'PBFA97CFB590B2093',
        'process_cpu_usage{instance=\"host.docker.internal:8060\"}',
      )+g.query.prometheus.withRefId('A')
      +g.query.prometheus.withRange(value=true)
    ])
    + g.panel.timeSeries.standardOptions.withUnit('percent')
    + g.panel.timeSeries.gridPos.withW(24)
    + g.panel.timeSeries.gridPos.withH(8),
  ])