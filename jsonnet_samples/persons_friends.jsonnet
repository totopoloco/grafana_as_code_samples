local defaultName = 'Agent Smith';

local Person(name=defaultName) = {
  name: name,
  welcome: 'Welcome ' + name + ' to the desert of the real',
};

{
  friends: {
    friend1: Person(name='Neo'),
    friend2: Person(name='Trinity'),
    friend3: Person(name='Morpheus'),
    friend4: Person(),
  },
}